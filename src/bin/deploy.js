const path = require('path');
const mustacheExpress = require('mustache-express');
const composer = require('../composer/composer')
const Mustache = require('mustache');
const fs = require('fs-extra');

// app.use('/assets', express.static(__dirname + '/public'));

const publicPath = path.join(__dirname, '../public');
const appBuild = path.join(__dirname, '../../dist');

const slides = composer();

const templateUri = path.join(__dirname, '../../template/template.hbs');

const template = fs.readFileSync(templateUri, "utf-8");

var view = {
  slides: slides
};

var output = Mustache.render(template, view);

function copyPublicFolder() {
  fs.copySync(publicPath, appBuild, {
    dereference: true,
    // filter: file => file !== paths.appHtml,
  });
}
fs.emptyDirSync(appBuild);

copyPublicFolder();

const outputFile = `${appBuild}/index.html`;

fs.writeFile(outputFile, output, function(err) {
  if(err) {
      return console.log(err);
  }

  console.log("The file was saved!", outputFile);
}); 

