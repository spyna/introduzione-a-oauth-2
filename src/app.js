const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const mustacheExpress = require('mustache-express');
const composer = require('./composer/composer')


const app = express();


app.use(favicon(path.join(__dirname, 'favicon.ico')));


//Register '.mustache' extension with The Mustache Express
app.engine('mustache', mustacheExpress());

app.set('view engine', 'mustache');

app.set('views', __dirname + '/views');

app.use('/assets', express.static(__dirname + '/public'));


app.get('/', (req, res) => {
  
  const slides = composer();
  
  res.render('index', {
    title: 'Introduzione ad Oauth 2.0',
    slides: slides
  });
});

module.exports = app;
