var fs = require('fs');
var path = require('path');
var marked = require('marked');

const dir = path.join(__dirname, '../../slides');

const composeSlide = () => {
  let markdownText = [];
  fs.readdirSync(dir).forEach(file => {
    let content = fs.readFileSync(path.join(dir, file), 'utf8');
    markdownText.push('---');
    markdownText.push(content);
  });

  return markdownText
    .join('\n')
    .split('---')
    .filter(function(v) {
      return v.replace(/\s/g, '');
    })
    .map(function(v) {
      return '<div>' + marked(v) + '</div>';
    })
    .join('\n');
} 

module.exports = composeSlide;