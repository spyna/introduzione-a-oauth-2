# Introduzione ad Oauth 2.0 

Questa è una presentazione fatta con una *Single Page Application* fatta con slide in *markdown* trasformate automaticamente in *html* con *node*. 

La presentazione è fatta usando parte di https://github.com/tmcw/big 

## installazione

```shell

git clone https://gitlab.com/spyna/introduzione-a-oauth-2.git && cd introduzione-a-oauth-2

yarn install

```

## Run locally 

```shell

yarn local

```

## Modifica delle slide

Le slides si trovano nella cartella `slides` e sono in formato *markdown*. Puoi modificare il file esistente o creare altri file, verranno aggiunti automaticamente.

### Divisione delle slides 

Per creare una nuova slide all'interno del file devi usare i caratteri `---`.

### Divisione dei file

Ogni file trovato nella cartella `slides` verrà parsato, l'ordine di inserimento nella presentazione è alfabetico. 

esempio: 

`slide-1.md` viene prima di `zlide-1.md` ma dopo `first-slide.md`. 
